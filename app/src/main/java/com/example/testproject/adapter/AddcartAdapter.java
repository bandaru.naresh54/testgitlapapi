package com.example.testproject.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.testproject.R;

import java.util.ArrayList;
import java.util.List;

public class AddcartAdapter extends RecyclerView.Adapter<AddcartAdapter.ViewHolder> {

    private List<String> mData;
    private LayoutInflater mInflater;
    private AddcartAdapter.ItemClickListener mClickListener;
    ArrayList<String> priceData = new ArrayList<>();


    // data is passed into the constructor
    public AddcartAdapter(Context context, List<String> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        priceData.add("100 Rs");
        priceData.add("200 Rs");
        priceData.add("300 Rs");
        priceData.add("400 Rs");
        priceData.add("500 Rs");
    }

    // inflates the row layout from xml when needed
    @Override
    public AddcartAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.addcart_rowview, parent, false);
        return new AddcartAdapter.ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(AddcartAdapter.ViewHolder holder, int position) {
        String animal = mData.get(position);
        holder.myTextView.setText(animal);
        holder.myTextViewprice.setText(priceData.get(position));
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView myTextView;
        TextView myTextViewprice;

        ViewHolder(View itemView) {
            super(itemView);
            myTextView = itemView.findViewById(R.id.name_txt1);
            myTextViewprice = itemView.findViewById(R.id.name_txt_price);
            itemView.setOnClickListener(this);
            myTextViewprice.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    public String getItem(int id) {
        return mData.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(AddcartAdapter.ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
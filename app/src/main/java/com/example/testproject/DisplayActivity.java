package com.example.testproject;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class DisplayActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);
        Bundle b = getIntent().getExtras();
        String productValue  = b.getString("product");
        ((TextView)findViewById(R.id.txt_name)).setText(productValue);
        findViewById(R.id.btn_checkout).setOnClickListener(new View.OnClickListener()
        {            @Override
            public void onClick(View v) {
            Intent i = new Intent(DisplayActivity.this, AddCartActivity.class);
            i.putExtra("product",productValue);
            startActivity(i);
            }
        });
    }
}
package com.example.testproject;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class CheckoutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);

      findViewById(R.id.btn_payment).setOnClickListener(new View.OnClickListener(){

          @Override
          public void onClick(View v) {
              Intent i = new Intent(CheckoutActivity.this, FinalScreenActivity.class);
              startActivity(i);
          }
      });
    }
}
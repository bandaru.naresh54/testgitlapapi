package com.example.testproject;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.testproject.adapter.MyRecyclerViewAdapter;

import java.util.ArrayList;

public class ProductActivity extends AppCompatActivity implements MyRecyclerViewAdapter.ItemClickListener {

    MyRecyclerViewAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        // data to populate the RecyclerView with
        ArrayList<String> animalNames = new ArrayList<>();
        animalNames.add("Baby Clothing");
        animalNames.add("Toys");
        animalNames.add("Cleaning essentials");
        animalNames.add("School supplies");
        animalNames.add("Your zone");
        animalNames.add("Reebok");
        animalNames.add("Tech");
        animalNames.add("Shoes");
        animalNames.add("Women's clothing");
        animalNames.add("Shoes");
        animalNames.add("Furniture");


        // set up the RecyclerView
        RecyclerView recyclerView = findViewById(R.id.rc_comments);
        LinearLayoutManager ll = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(ll);
        adapter = new MyRecyclerViewAdapter(this, animalNames);
        adapter.setClickListener(this);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                ll.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onItemClick(View view, int position) {

        Intent i = new Intent(this, DisplayActivity.class);
        i.putExtra("product",adapter.getItem(position));
        startActivity(i);
       // Toast.makeText(this, "You clicked " + adapter.getItem(position) + " on row number " + position, Toast.LENGTH_SHORT).show();
    }
}

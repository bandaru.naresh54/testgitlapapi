package com.example.testproject;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.testproject.adapter.AddcartAdapter;
import com.example.testproject.adapter.MyRecyclerViewAdapter;

import java.util.ArrayList;

public class AddCartActivity extends AppCompatActivity implements AddcartAdapter.ItemClickListener {


    AddcartAdapter adapter;
    ArrayList<String> animalNames = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addcart);
        Bundle b = getIntent().getExtras();
        String productValue  = b.getString("product");
        // data to populate the RecyclerView with
        animalNames.add(productValue);
        animalNames.add("Baby Clothing");
        animalNames.add("Toys");
        animalNames.add("Shoes");
        animalNames.add("School supplies");


        // set up the RecyclerView
        RecyclerView recyclerView = findViewById(R.id.rc_details);
        LinearLayoutManager ll = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(ll);
        adapter = new AddcartAdapter(this, animalNames);
        adapter.setClickListener(this);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                ll.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setAdapter(adapter);
        findViewById(R.id.btn_checkout).setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                Intent i = new Intent(AddCartActivity.this, CheckoutActivity.class);
             //   i.putExtra("product",adapter.getItem(position));

                startActivity(i);
            }
        });
    }
    @Override
    public void onItemClick(View view, int position) {

      /*  Intent i = new Intent(this, CheckoutActivity.class);
        i.putExtra("product",adapter.getItem(position));

        startActivity(i);*/
        // Toast.makeText(this, "You clicked " + adapter.getItem(position) + " on row number " + position, Toast.LENGTH_SHORT).show();
    }

}